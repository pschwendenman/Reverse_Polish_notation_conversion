#include <stdlib.h>
#include <check.h>

#include "../src/rpn.h"

Suite * make_rpn_suite(void) {
    Suite *s;
    TCase *tc_sum;

    s = suite_create("rpn");

    /* Core test case */
    tc_sum = tcase_create("rpn");

    suite_add_tcase(s, tc_sum);

    return s;
}

int main(void) {
    int number_failed;
    SRunner *sr;

    sr = srunner_create(make_rpn_suite());

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
