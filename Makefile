CC = gcc
CFLAGS = --pedantic -Wall -std=c99 -O2
LIBS = `pkg-config --libs check`

TEST = tests/check_rpn.c
OBJS = rpn.o check_rpn.o

%.o: src/%.c src/%.h
	gcc -c $(CFLAGS) -o $@ $<

check_rpn.o: tests/check_rpn.c
	gcc -c $(CFLAGS) -o $@ $<

.PHONY: test check clean

test: check

check: check_rpn
	@./check_rpn

check_rpn: ${OBJS}
	$(CC) $(CFLAGS) -o $@ ${OBJS} $(LIBS)

clean:
	rm -f $(OBJS) check_rpn
